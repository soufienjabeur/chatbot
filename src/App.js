import React from 'react';
import ChatBot from 'react-simple-chatbot';
import { ThemeProvider } from 'styled-components';
import './App.css';
import BackToTop from './components/navbar/BackToTop';


const App = () => {

    const theme = {
        background: '#f5f8fb',
        fontFamily: 'Source Sans Pro',
        headerBgColor: '#4988E3',
        headerFontColor: '#fff',
        headerFontSize: '15px',
        botBubbleColor: '#4988E3',
        botFontColor: '#fff',
        userBubbleColor: '#fff',
        userFontColor: '#4a4a4a',

    }

    const steps = [

        {
            id:'0',
            message: 'Bonjour ! Vous appelez comment ?',
            trigger:'1'
        },
        {
            id: '1',
            user: true,
            trigger: '2',
          },
        {
            id: '2',
            message:'{previousValue}, Je réponds à vos questions sur la douane : gestion de vos comptes, démarches, viticulture... Interrogez-moi sans réserve !' ,
            trigger: '3'
        },
        {
            id: '3',
            options: [
                { value: 1, label: 'Voire les questions les plus fréquentes', trigger:'4' },
                { value: 2, label: 'Infos Douane Service', trigger:'5' },
                { value: 3, label: 'Demande d\'assistance', trigger:'6' },
            ],
        },
        {
            id:'4',
            component:(<div>Vous pouvez consulter le FAQ en visitant cette page <a href="https://www.douane.gouv.fr/FAQ" target="_blanck">Ici</a></div>),
            trigger:'3'
            
        },
        {
            id:'5',
            component:(<div>Pour plus d'infos sur le service de douane, vous pouvez visiter cette page <a href="https://www.douane.gouv.fr/services-aide/infos-douane-service" target="_blanck">Ici</a></div>),
            trigger:'3'
            
        },
        {
            id:'6',
            component:(<div>Pour demander l'aide d'un assistance, vous pouvez visiter cette page <a href="https://www.douane.gouv.fr/service-en-ligne/outil-en-ligne-de-gestion-de-lassistance-olga" target="_blanck">Ici</a></div>),
            trigger:'3'
            
        },

        
    ]
    return (
        <div>
            
            <div className="container">
                <ThemeProvider theme={theme}>
                    <ChatBot headerTitle={<div><h4 style={{ marginBottom: '0px', paddingBottom: '0px' }}>DouBot</h4> <h6 style={{ marginTop: '0px' }}>Votre assistant virtuel Douane</h6></div>}
                        steps={steps}
                    />
                </ThemeProvider>
            </div>

        </div>
    )
}

export default App
